## Sobre

Exploramos o conceito de massa a partir de um ponto de vista extrínseco. Nossa
abordagem baseia-se na introdução de uma classe especial de imersões em espaços
euclidianos e no desenvolvimento de uma identidade integral que pode ser vista
como uma versão das clássicas fórmulas de Hsiung-Minkowski para uma classe
especial de subvariedades imersas não compactas. A partir dessa identidade,
deduzimos uma desigualdade geométrica que deve ser satisfeita sempre que
a conjectura da massa positiva para a massa GBC (e a massa ADM, em particular)
for válida.

Até onde sabemos, esta é a primeira vez que essa classe de imersões e essa
identidade integral são descritas na literatura. Ela foi construída para nos
permitir descrever o comportamento assintótico da imersão e aplicar o teorema
da divergência para deduzir a identidade integral. Por essa razão, uma imersão
nesta classe é dita ser uma _imersão assintoticamente euclidiana_.

Também exploramos as condições necessárias para que campos vetoriais gerem
as cargas assintóticas de interesse e apresentamos duas conjecturas. Em uma,
conjecturamos que espaços assintoticamente euclidianos admitem uma imersão
isométrica assintoticamente euclidiana; na outra, conjecturamos que a desigualdade
geométrica deve ser válida sempre que uma imersão assintoticamente euclidiana
satisfizer uma hipótese natural. Se ambas as conjecturas forem válidas,
a conjectura da massa positiva para a massa GBC (e massa ADM, em particular) pode
ser deduzida a partir de nossa abordagem.


## Licença

O conteúdo do projeto em si está licenciado sob os termos da
[licença Creative Commons Atribuição 4.0](https://creativecommons.org/licenses/by/4.0/deed.pt_BR),
e o código subjacente usado para formatar e apresentar o conteúdo está
licenciado sob os termos da [LPPL 1.3c+](./LICENSE.LPPL-1.3c.md).

