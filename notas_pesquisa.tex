%% filename: notas_pesquisa.tex
%% 
%% Copyright 2019 Alexandre de Sousa and Frederico Girão.
%% 
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%% http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2005/12/01 or later.
%% 
%% ====================================================================


\documentclass[12pt]{article}

%% Provê os pacotes e comandos usados no presente projeto
\usepackage{./latex_config/custom_preamble}

\title{Calculando a energia ADM e outras cargas assintóticas}
\author{Alexadre "asm"}
\date{}

\begin{document}
\maketitle
\flushbottom
\newpage
\pagestyle{fancynotes}


\section{Energia ADM}

\begin{definition} \label{def: energia ADM}
  \begin{align}
    E(M,g) := - c_m \int_{\Sigma_{\infty}} G\left( X, \nu \right) d\sigma_g \label{def:carga}
  \end{align}
  \begin{description}
  \item[$(M^m, g)$] variedade riemanianna completa, assintoticamente plana
    à taxa $\tau > \frac{m-2}{2}$
  \item[$c_m$] $\equiv \frac{1}{(m-1)(m-2)\omega_{m-1}}$, onde $\omega_{m-1} \equiv |\mathbf{S}^{m-1}|$
  \item[$X$] $= x^i \frac{\partial}{\partial x^i}$ é o campo definido nos fins,
    via ``pullback'' da dilatação radial do espaço euclidiano
  \item[$\nu$] campo normal unitário apontando pro infinito
  \end{description}
\end{definition}

\begin{remark}
  O campo $X$ está definido apenas na região assintótica \\
  $\Longrightarrow$  Não podemos aplicar o teorema de Stokes
  \begin{description}
    \item[\textbf{Uma solução}:] encontrar um campo $Y \in \Gamma\left( TM
      \right)$, globalmente definido, que desempenhe o mesmo papel do campo $X$.
  \end{description}
\end{remark}


\subsection{Condição suficiente para uma extensão assintótica global da dilatação radial nos fins}

Em cada um dos fins, temos:
\begin{align*}
  \left\vert \int_{S_r} G(Y, \nu) d\sigma_g - \int_{S_r} G(X, \nu) d\sigma_g \right\vert
  & = \left\vert \int_{S_r} G(Y-X, \nu) d\sigma_g \right\vert \\
  & \le \left\vert \int_{S_r} G(Y-X, \nu - \eta) d\sigma_g \right\vert
    + \left\vert \int_{S_r} G(Y-X, \eta) d\sigma_g \right\vert \\
  & \le \int_{S_r} |G_{ij}| |Y^i - X^i| |\nu^j - \eta^j| d\sigma_g
    + \int_{S_r} |G_{ij}| |Y^i - X^i| |\eta^j| d\sigma_g \\
  & = \int_{S_r} |G_{ij}| |Y^i - X^i| \left( |\nu^j - \eta^j| + |\eta^j| \right) d\sigma_g
\end{align*}
\begin{description}
\item[\( S_r \)] denota as esferas coordenadas nos fins
\item [\(\eta\)] denota o campo normal unitário, com respeito a métrica euclidiana (nos fins)
\end{description}

Haja visto que o tensor de Einstein é um polinômio de primeira ordem
no tensor de Riemmann,
\begin{align}
  G & = Rc - {R \over 2} g \nonumber \\
    & = \frac{1}{4} g_{ac} \delta_{b j_1 j_2}^{c i_1 i_2}
      \tensor[]{R}{_{i_1 i_2}^{j_1 j_2}}
      \label{eq:tensor_einstein},
\end{align}
que por sua vez é polinomial na métrica e suas derivadas até segunda ordem,
\todo[noline]{Esclarecer notação? Ref.: [c01-p174]}
\begin{align*}
  R\indices{^l_{ijk}} = g^{lr} \left[ -\Gamma_{ri[j,k]}(\partial^2g) + g^{pq} Q_{rp[j|q|k]i}(\partial g) \right],
\end{align*}
onde
\begin{itemize}
\item [$\Gamma_{rij}$] denota o símbolo de Christoffel de primeiro tipo,
  \begin{align*}
    \Gamma_{rij} = {1 \over 2} \left\{ g_{ri,j} + g_{rj,i} - g_{ij,r} \right\}
  \end{align*}
\item [$Q_{rp[j|q|k]i}$] denota uma expressão quadrática nas primeiras derivadas da métrica,
  \begin{align*}
    Q_{rp[j|q|k]i} = \Gamma_{rp\left[ j \right.}\Gamma_{|q|\left. k \right]i} - g_{rp,\left[ j \right.}\Gamma_{|q|\left. k \right]i}
  \end{align*}
\end{itemize}

Haja visto também que, \label{pg:passagemCampoGlobal}
\begin{align*}
  g_{ij} & = \delta_{ij} + \sigma_{ij}, \text{ com } \sigma_{ij}(x) = O_2(|x|^{-\tau}), \ |x| \to \infty \\
  \Longrightarrow g^{ij} & = \delta^{ij} + \theta^{ij}, \text{ com } \theta^{ij}(x) = O_2(|x|^{-\tau}), \ |x| \to \infty
\end{align*}
segue que,
\begin{align*}
  R\indices{^l_{ijk}} (x) & = O(|x|^{-\tau - 2}), \ |x| \to \infty \\
  \Longrightarrow G_{ij}(x) & = O(|x|^{-\tau - 2}), \ |x| \to \infty
\end{align*}
Ademais,
\todo{Ref.: [c01-pg179]}
\begin{align*}
  \nu^i & = \eta^i + v^i, \ v^i(x) = O_2(|x|^{-\tau}), \ |x| \to \infty \\
  d\sigma_g & = ( 1 + w)d\sigma_e, \ w(x) = O_2(|x|^{-\tau}), \ |x| \to \infty,
\end{align*}

Assim, para $r >> 1$ temos:
\begin{align*}
  \left\vert \int_{S_r} G(Y, \nu) d\sigma_g - \int_{S_r} G(X, \nu) d\sigma_g \right\vert
  & \le \int_{S_r} |G_{ij}| |Y^i - X^i| \left( |\nu^j - \eta^j| + |\eta^j| \right) (1 + w) d\sigma_e \\
  & \le C_1 r^{-\tau - 2} \ \sup_{S_r}|Y^i - X^i| (C_2 r^{-\tau} + 1) (1 + C_3 r^{-\tau}) \omega_{m-1} r^{m-1} \\
  & = C_4 r^{-[\tau + 2 - (m -1)]} (1 + r^{-\tau})^2 \sup_{S_r}|Y^i - X^i|
\end{align*}

\begin{margintable}\vspace{.8in}\footnotesize
  \begin{tabularx}{\marginparwidth}{|X}
    Condição assintótica para a extensão global
  \end{tabularx}
\end{margintable}
Suponhamos que, com respeito a uma carta escolhida, temos:
\begin{align*}
  \left( Y^i - X^i \right)(x)  = O(|x|^{-q}), \ |x| \to \infty, \ q \in \bbR \\
  \Longrightarrow \left\vert \int_{S_r} G(Y, \nu) d\sigma_g - \int_{S_r} G(X, \nu) d\sigma_g \right\vert
  \le C_4 r^{-[\tau + 2 - (m - 1) +q]}(1 + r^{-\tau})^2
\end{align*}
Para que o lado esquerdo se anule, quando passarmos ao limite,
é suficiente tomarmos,
\begin{align*}
  s \equiv \tau + 2 - (m - 1) + q > 0
\end{align*}
Haja visto que,
\begin{align*}
  \tau > \tau_c \Longrightarrow s > \tau_c + 2 - (m - 1) + q
\end{align*}
é suficiente tomarmos
\begin{align*}
  \tau_c + 2 - (m - 1) + q \ge 0 \Longleftrightarrow q \ge q_c \equiv m - 1 -(\tau_c + 2)
\end{align*}
Dessa forma, obtemos:
\begin{sumario} \label{sumario:energia_ADM--campo_global}
  Seja \( (M^m, g) \) uma variedade riemanniana completa, possivelmente com fronteira,
  assintoticamente euclidiana à taxa \( \tau > \tau_c = {m - 2 \over 2}\). Se $M$ admite
  um campo global suave \( Y \in \Gamma \left( TM \right) \) e uma carta nos fins,
  que satisfazem a condição assintótica:
  \begin{align*}
  \left( Y^i - X^i \right)(x)  = O(|x|^{-q}), \ |x| \to \infty
  \end{align*}
  com \( q \ge q_c = m -1 - (\tau_c +2) = {m - 4 \over 2} \). Então, temos:
\begin{align}
  E(M,g) & = - c_m \int_{S_{\infty}} G\left( Y, \nu \right) d\sigma_g
           \nonumber \\
         & = - \frac{c_m}{2} \int_M \langle{G, \mcL_Y g}  \rangle d\mu_g
           - c_m \int_{\partial M} G\left( Y, \nu \right) d\sigma_g
           \nonumber \\
         & = - \frac{c_m}{2} \int_M \langle{G, \mcL_Y g}  \rangle d\mu_g
         + c_m \int_{\partial M} \langle{Y, \nu} \rangle_g
           \left( {\sc{h} \over 2} + \sigma_2(B_{\nu}) \right) d\sigma_h
           \nonumber \\
         & + \frac{c_m}{2}\int_{\partial M} \langle{T_1\left( B_{\nu} \right),
           \iota^{\ast}\mcL_Y g}  \rangle_h d\sigma_h
           \label{eq:energia_ADM--formula_intrinseca}
\end{align}
onde \( h = \iota^{\ast}g \), com \( \iota: \partial M \hookrightarrow M \).
\end{sumario}

\begin{proof}
  A primeira igualdade segue da discussão anterior.

  Por cálculo direto, obtemos:
  \begin{lemma}
    Seja \( (M, g) \) uma variedade riemanniana
    e \( \protect{\left( E, \tensor[^E]{\nabla}{} \right) \xrightarrow{\pi} M} \)
    um fibrado vetorial munido com uma conexão linear. Se \( X \in TM \)
    e \( T \in \Gamma\left( E \otimes^2 T^{\ast}M \right) \),
    então vale a identidade:
    \begin{align} \label{eq:divergencia--forma_bilinear}
      \dvg \left( \iota_X T \right) &= \iota_X \dvg \, \tensor[^{t}]{T}{}
        + \tr_g^2 \left( T \otimes \nabla X ^{\flat}\right)
    \end{align}

    Ademais, se \( T \) é simétrico, i.e. \( \tensor[^{t}]{T}{} = T \), então
    \begin{align} \label{eq:divergencia--forma_bilinear_simetrica}
      \dvg \left( \iota_X T \right) &= \iota_X \dvg \, T
                                      + {1 \over 2} \tr_g^2 \left( T \otimes \mcL_Xg \right)
    \end{align}
  \end{lemma}

  Logo, combinando esse lema com o teorema de Stokes, aplicado a uma exaustão por
  domínios compactos escolhida adequadamente, e usando os fatos de que
  \[ G \in \Gamma\left( \odot^2 T^{\ast}M \right) \textrm{ e } \dvg \, G = 0 \]
  obtemos a segunda igualdade.

  Das equações de Gauss e Mainardi-Codazzi segue que:
  \begin{lemma}
    Seja \( \phi: \left( N^{m-1}, h \right) \looparrowright \left( M^m, g \right) \)
    uma \emph{imersão isométrica}. Se \( \xi \in \Gamma TU^{\perp}_1 \subset \Gamma TN^{\perp}_1 \)
    denota uma escolha de um campo normal unitário local em uma vizinhança \( U \subset N \),
    então nessa vizinhança valem as identidades:
    \begin{align*}
      \iota_{\xi} \iota_{\xi}\tensor[^g]{G}{} & = -\frac{\tensor[^{h}]{R}{}}{2}
                                                + \sigma_2(B_{\xi}) \\
      \phi^{\ast}\iota_{\xi} \tensor[^g]{G}{} & = \dvg_{h}\left[ T_1(B_{\xi}) \right]
    \end{align*}
  \end{lemma}

  \begin{description}
  \item[\( \sigma_1\left( B_{\xi} \right) = tr_{h} B_{\xi} \)]
    primeira função simétrica elementar
  \item[\( \sigma_2\left( B_{\xi} \right) = \frac{\left[ \sigma_1\left( B_{\xi} \right) \right]^2
      - |B_{\xi}|^2_{h}}{2} = - \frac{|\ring{B}_{\xi}|^2_h}{2} \)] segunda função simétrica elementar
  \item[\( T_1(B_{\xi}) = \sigma_1\left( B_{\xi} \right) h - B_{\xi} \)]
    primeira transformação de Newton
  \end{description}

  Assim,
  \begin{align*}
    G(Y, \nu) & = \langle{Y, \nu}  \rangle G(\nu, \nu) + G(Y^{\top}, \nu) \\
              & = \langle{Y, \nu} \rangle \left( -\frac{\tensor[^{h}]{R}{}}{2}
                + \sigma_2(B_{\nu}) \right) + \dvg_{h}\left[ T_1(B_{\nu}) \right](Y^{\top})
  \end{align*}
  Usando a identidade \eqref{eq:divergencia--forma_bilinear_simetrica}
  e os fatos de que
  \begin{align*}
    & T_1\left( B_{\nu} \right) \in \Gamma\left( \odot^2 T^{\ast}M \right) \\
    & \iota : \partial M \hookrightarrow M \\
    & h = \iota^{\ast} g
  \end{align*}
  obtemos:
  \begin{align*}
    G(Y, \nu) & = \langle{Y, \nu} \rangle_g \left( -\frac{\tensor[^{h}]{R}{}}{2}
                + \sigma_2(B_{\nu}) \right)
                + \dvg_{h}\left[ \iota_{Y^{\top}} T_1(B_{\nu}) \right]
                - {1 \over 2} \langle{T_1\left( B_{\nu} \right), \mcL_{Y^{\top}} h}  \rangle_h
  \end{align*}

  \begin{lemma} \label{lemma:derivada--Lie}
    Seja \( \phi: \left( N^n, h \right) \looparrowright \left( \overline{M}^m, \overline{g} \right) \)
    uma \emph{imersão isométrica}.
    Se \( \overline{Z} \in \Gamma \left( \phi^{\ast}T\overline{M} \right) \)
    e \( X \in \Gamma TN \) são tais que
    \[ \overline{Z} = \phi_{\ast}X + \overline{Z}^{\perp} \]
    então vale a identidade:
    \marginnote{%
      Veja a argumentação que precede a identidade~\eqref{eq:derivada--Lie}}
    \begin{align*}
      \mcL_X h = \phi^{\ast} \mcL_{\overline{Z}} \overline{g} + 2 \overline{B}_{\overline{Z}}
    \end{align*}
  \end{lemma}

  Combinando esse lema com a identidade (veja lema~\ref{lema:expansao_Laplace})
  \[ \langle{T_1\left( B_{\nu} \right), B_{\nu}}  \rangle_h
    = 2 \sigma_2\left( B_{\nu} \right) \]
  obtemos:
  \begin{align*}
    - {1 \over 2} \langle{T_1\left( B_{\nu} \right), \mcL_{Y^{\top}} h}  \rangle_h
    & = - {1 \over 2} \langle{T_1\left( B_{\nu} \right), \iota^{\ast}\mcL_Y g}  \rangle_h
      - \langle{T_1\left( B_{\nu} \right), B_Y}  \rangle_h \\
    & = - {1 \over 2} \langle{T_1\left( B_{\nu} \right), \iota^{\ast}\mcL_Y g}  \rangle_h
      - 2\langle{Y, \nu}  \rangle_g \sigma_2\left( B_{\nu} \right)
  \end{align*}
  Logo,
  \begin{align*}
  G(Y, \nu) & = \langle{Y, \nu} \rangle_g \left( - \frac{\tensor[^h]{R}{}}{2}
              - \sigma_2(B_{\nu}) \right)
              - {1 \over 2} \langle{T_1\left( B_{\nu} \right), \iota^{\ast}\mcL_Y g}  \rangle_h \\
            & + \dvg_h\left[ \iota_{Y^{\top}} T_1(B_{\nu}) \right] \\
            & = - \langle{Y, \nu} \rangle_g \left( \frac{\tensor[^h]{R}{}}{2}
              + \sigma_2(B_{\nu}) \right)
              - {1 \over 2} \langle{T_1\left( B_{\nu} \right), \iota^{\ast}\mcL_Y g}  \rangle_h \\
            & + \dvg_h\left[ \iota_{Y^{\top}} T_1(B_{\nu}) \right] \\
  \end{align*}
  Haja visto que \( \partial^2 M = \emptyset \), segue que:
  \begin{align*}
    \int_{\partial M} G(Y, \nu) d\sigma_g
    & = - \int_{\partial M} \langle{Y, \nu} \rangle_g
      \left( {\sc{h} \over 2} + \sigma_2(B_{\nu}) \right) d\sigma_h
      - {1 \over 2} \int_{\partial M} \langle{T_1\left( B_{\nu} \right),
      \iota^{\ast}\mcL_Y g}  \rangle_h d\sigma_h
  \end{align*}
  Donde segue a terceira igualdade.
\end{proof}

\todo[noline]{Aplicar a expansão assintótica das transições de cartas nos fins}
\begin{remark}
  A condição assintótica sobre a extensão global, em princípio,
  depende da carta escolhida nos fins. Também é importante verificar se os
  resultados obtidos até esse ponto, são invariantes com respeito
  ao atlas (maximal) das cartas admissíveis, no sentido da boa definição
  da energia ADM.
\end{remark}

Haja visto que, a geradora infinitesimal da dilatação radial é um \emph{campo gradiente},
à saber, o \emph{gradiente do quadrado distância euclidiana},
\begin{align*}
  X = \tensor[^{\delta}]{\grad}{} \left( {|x|_{\delta}^2 \over 2} \right).
\end{align*}
Somos motivados a tomar a sua extensão assintótica global também como sendo um campo gradiente,
\todo{Verificar regularidade mínima necessária}
\begin{align*}
  Y = \tensor[^g]{\grad}{} f \in \Gamma\left( TM \right).
\end{align*}
Note que, com respeito a carta fixada, temos:
\begin{align*}
  Y^i = g^{ij} \partial_j f = (\delta^{ij} + \theta^{ij}) \partial_j f \\
  \Rightarrow  \left( Y - X \right)^i(x) =  \partial_i \left( f - {|x|_{\delta}^2 \over 2} \right)
                              + \theta^{ij} \partial_j f
\end{align*}
Logo, pelo lema \ref{sumario:energia_ADM--campo_global},
é suficiente tomarmos uma função satisfazendo a expansão assintótica,
\begin{align*}
  \partial_i \left( f - {|x|_{\delta}^2 \over 2} \right)(x) = O(|x|^{-q}),
  \forall i \in I_m = \{1, \ldots, m\}
\end{align*}
com \( q \ge q_c = m - 3 - \tau_c \). De fato, aplicando a expansão assintótica da métrica, obtemos:
\begin{align*}
  \partial_i\left( {|x|_{\delta}^2 \over 2} \right) = x^i
  \Longrightarrow \partial_i f = x^i + O(|x|^{-q}) \\
  \Longrightarrow \theta^{ij} \partial_j f = O(|x|^{-(\tau - 1)})
\end{align*}
Haja visto que \( \tau > \tau_c = \frac{m - 2}{2} \), segue que \( \tau - 1 > \tau_c - 1 = \frac{m - 4}{2} = q_c \). Portanto,
\begin{align*}
  \partial_i \left( f - {|x|_{\delta}^2 \over 2} \right)(x) = O(|x|^{-q})
  \Longrightarrow \left( Y - X \right)^i(x) = O(|x|^{-q}) \quad \forall i \in I_m = \{1, \ldots, m\}
\end{align*}
com \( q \ge q_c = m - 3 - \tau_c \). Assim, obtemos o seguinte resultado:
\todo{Investigar relação entre a energia ADM e os autovalores do laplaciano}
\begin{sumario} \label{sumario:energia_ADM--formula_intrinseca--campo_gradiente}
  Seja \( (M^m, g) \) uma variedade riemanniana completa, possivelmente com fronteira,
  assintoticamente euclidiana à taxa \( \tau > \tau_c = {m - 2 \over 2}\). Se $M$ admite
  \( f \in C^{\infty}(M) \) e uma carta nos fins, que satisfazem a condição assintótica:
  \begin{align*}
  \left( f - \frac{|x|_{\delta}^2}{2} \right)(x)  = O_1(|x|^{-q + 1}), \ |x| \to \infty
  \end{align*}
  com \( q \ge q_c = m - 3 - \tau_c = {m - 4 \over 2} \). Daí obtemos o seguinte resultado:
\begin{align}
  E(M,g) & = - c_m \int_M \langle{G, \tensor[^g]{\grad}{^2} f}  \rangle d\mu_g
         + c_m \int_{\partial M} \frac{\partial f}{\partial \nu}
           \left( {\sc{h} \over 2} - 3\sigma_2(B_{\nu}) \right) d\sigma_h
           \nonumber \\
         & + c_m \int_{\partial M} \langle{T_1\left( B_{\nu} \right),
           \iota^{\ast} \tensor[^g]{\grad}{^2} f} \rangle_h d\sigma_h
           \nonumber \\
         & = - c_m \int_M \langle{G, \tensor[^g]{\grad}{^2} f}  \rangle d\mu_g
         + c_m \int_{\partial M} \frac{\partial f}{\partial \nu}
           \left( {\sc{h} \over 2} - 5 \sigma_2(B_{\nu}) \right) d\sigma_h
           \nonumber \\
         & + c_m \int_{\partial M} \langle{T_1\left( B_{\nu} \right),
           \tensor[^h]{\grad}{^2} f} \rangle_h d\sigma_h
           \nonumber
\end{align}
onde \( h = \iota^{\ast}g \), com \( \iota: \partial M \hookrightarrow M \).
\end{sumario}
\begin{proof}
  A condição assintótica segue da discussão precedente.

  A primeira igualdade segue por substituição direta da identidade,
  \[ \mcL_{\tensor[^g]{\grad}{} f} g = 2 \tensor[^g]{\grad}{^2} f \]
  na fórmula \ref{eq:energia_ADM--formula_intrinseca}.

  A segunda igualdade segue por aplicação direta das seguintes identidades:
  \begin{align*}
    \iota^{\ast} \tensor[^g]{\grad}{^2} f
    = \tensor[^h]{\grad}{^2} f - \langle{B, \tensor[^g]{\grad}{} f}  \rangle_g
    = \tensor[^h]{\grad}{^2} f - \frac{\partial f}{\partial \nu} B_{\nu}
  \end{align*}
  \begin{align*}
    \langle{T_1\left( B_{\nu} \right), B_{\nu}} \rangle_h = 2 \sigma_2\left( B_{\nu} \right)
  \end{align*}
  Esta última é um caso particular da expansão de Laplace
  (veja lema \ref{lema:expansao_Laplace}).
\end{proof}


\subsection{Aplicação: Mergulho isométrico}
\label{sec:mergulho_isometrico}

\begin{remark}[Teorema do mergulho isométrico de Nash]
  \( \forall \, (M^m, g) \quad C^{\infty} \) \\
  \( \exists \quad u: (M^m, g) \hookrightarrow \left( \overline{\bbR}^{d_m},
    \overline{\delta} \right) \) mergulho isométrico \( C^{\infty} \)
\end{remark}

A linha de raciocínio que precede o sumário
\ref{sumario:energia_ADM--formula_intrinseca--campo_gradiente}, sugere o campo global
\begin{align*}
  Y = \tensor[^g]{\grad}{}\left( {|u|_{\overline{\delta}}^2 \over 2} \right)
  \in \Gamma\left( TM \right)
\end{align*}
como uma extensão assintótica global apropriada para o campo \( X \). Assim,
é suficiente tomarmos um mergulho isométrico tal que
\begin{margintable}\vspace{.5in}\footnotesize
  \begin{tabularx}{\marginparwidth}{|X}
    Condição assintótica sobre o mergulho isométrico
  \end{tabularx}
\end{margintable}
%
\begin{align*}
  \partial_i\left( {|u|_{\overline{\delta}}^2 \over 2} \right) = x^i + O(|x|^{-q}),
  \forall i \in I_m = \{1, \ldots, m\}
\end{align*}
com \( q \ge q_c = m -1 - (\tau_c + 2) \).

Note também que,
\todo{Citar inspiração na seção 8 do texto
  \href{https://arxiv.org/abs/math/0311352}{[arXiv:math/0311352]}}
\begin{align*}
  u_{\ast}\left[ \tensor[^g]{\grad}{}\left( {|u|_{\overline{\delta}}^2 \over 2} \right) \right]
  = \tensor[^{\overline{\delta}}]{\grad}{}\left( {|u|_{\overline{\delta}}^2 \over 2} \right)^{\top}
  = u^{\top}
\end{align*}

Assim,
\begin{margintable}\vspace{.5in}\footnotesize
  \begin{tabularx}{\marginparwidth}{|X}
    \( \overline{B}_{\overline{Z}} = - {1 \over 2} u^{\ast}\left( \mcL_{\overline{Z}^{\perp}} \overline{\delta} \right) \) \\
    curvatura extrínseca na direção \( \overline{Z} \)
  \end{tabularx}
\end{margintable}
\begin{align}
  \overline{Z} &\equiv u \in \Gamma\left( u^{\ast}T\bbR^{d_m} \right) \Longrightarrow \overline{Z}
      = u_{\ast}Y + \overline{Z}^{\perp} \nonumber \\
  \Longrightarrow \mcL_Y g &= \mcL_Y u^{\ast}\overline{\delta}
                             = u^{\ast}\left( \mcL_{u_{\ast}Y}\overline{\delta} \right)
                             \nonumber \\
               &= u^{\ast}\left( \mcL_{\overline{Z}} \overline{\delta}
                 - \mcL_{\overline{Z}^{\perp}}\overline{\delta}  \right)
                 = u^{\ast}\left( \mcL_{\overline{Z}} \overline{\delta} \right)
                 + 2 \overline{B}_{\overline{Z}} \label{eq:derivada--Lie}
\end{align}
Assim,
\begin{margintable}\vspace{.5in}\footnotesize
  \begin{tabularx}{\marginparwidth}{|X}
    \( \mcL_{\overline{Z}} \overline{\delta} =  \frac{\dvg_{\overline{\delta}} \overline{Z}}{d_m} \, \overline{\delta} \) \\
    \( \dvg_{\overline{\delta}} \overline{Z} = d_m \)
  \end{tabularx}
\end{margintable}
\begin{align*}
  u^{\ast}\left( \mcL_{\overline{Z}} \overline{\delta} \right) &= u^{\ast}\overline{\delta} = g \\
  \Longrightarrow \mcL_Y g &= g + 2 \overline{B}_{\overline{Z}}
\end{align*}

Daí,
\begin{margintable}\vspace{.5in}\footnotesize
  \begin{tabularx}{\marginparwidth}{|X}
    \( \tr_g G = -\frac{m - 2}{2} R \) \\
    \( b_m = \frac{m - 2}{4} c_m \)
  \end{tabularx}
\end{margintable}
\begin{align}
  \langle{G, \mcL_Y g}  \rangle
  & = \tr_g G + 2\langle{G, \overline{B}_{\overline{Z}}}  \rangle
    \label{eq:derivadaLie} \\
  \Longleftrightarrow - {c_m \over 2}\langle{G, \mcL_Y g}  \rangle
  & = b_m R - c_m \langle{G, \overline{B}_{\overline{Z}}}  \rangle \nonumber
\end{align}

\begin{margintable}\vspace{.9in}\footnotesize
  \begin{tabularx}{\marginparwidth}{|X}
    \( \iota^{\ast}g = h \) \\
    Na segunda igualdade usamos a identidade de Girard-Newton
    (Vide Lema \ref{lema:identidade_Girard-Newton})
  \end{tabularx}
\end{margintable}
\begin{align*}
\langle{T_1\left( B_{\nu} \right), \iota^{\ast}\mcL_Y g}  \rangle_h
  & = \langle{T_1\left( B_{\nu} \right), \iota^{\ast}g}  \rangle_h
    + 2\langle{T_1\left( B_{\nu} \right), \iota^{\ast}\overline{B}_{\overline{Z}}}  \rangle_h \\
  & = (m - 1) \sigma_1\left( B_{\nu} \right)
    + 2\langle{T_1\left( B_{\nu} \right), \iota^{\ast}\overline{B}_{\overline{Z}}}  \rangle_h \\
  \Longleftrightarrow {c_m \over 2 } \langle{T_1\left( B_{\nu} \right), \iota^{\ast}\mcL_Y g}  \rangle_h
  & = \frac{m - 1}{2} c_m \sigma_1\left( B_{\nu} \right)
    + c_m \langle{T_1\left( B_{\nu} \right), \iota^{\ast}\overline{B}_{\overline{Z}}}  \rangle_h
\end{align*}
Substituindo na fórmula \eqref{eq:energia_ADM--formula_intrinseca}, obtemos:
\begin{align*}
  E(M,g) & = \int_M \left( b_m R - c_m \langle{G, \overline{B}_{\overline{Z}}}
           \rangle \right) d\mu_g
           + c_m \int_{\partial M} \langle{Y, \nu} \rangle_g
           \left( {\sc{h} \over 2} + \sigma_2(B_{\nu}) \right) d\sigma_h \\
         & + \frac{m - 1}{2} c_m \int_{\partial M} \sigma_1\left( B_{\nu} \right) d\sigma_h
           + c_m \int_{\partial M} \langle{T_1\left( B_{\nu} \right),
           \iota^{\ast}\overline{B}_{\overline{Z}}} \rangle_h d\sigma_h
\end{align*}

Dessa forma, obtemos:
\begin{sumario} \label{sumario:energia_ADM--mergulho_isometrico}
  Seja \( (M^m, g) \) uma variedade riemanniana completa,
  possivelmente com fronteira, assintoticamente euclidiana à taxa
  \( \protect{\tau > \tau_c = {m - 2 \over 2}} \).
  Se $M$ admite um mergulho isométrico suave
  \[
    u: (M^m, g) \hookrightarrow \left( \overline{\bbR}^{d_m}, \overline{\delta} \right)
  \]
  e uma carta nos fins, que satisfazem a condição assintótica,
  \begin{align*}
   \left( |u|_{\overline{\delta}}^2 - |x|_{\delta}^2 \right)(x) = O_1(|x|^{-q + 1}),
  \end{align*}
  com \( q \ge q_c = m -1 - (\tau_c + 2) \). Então, temos:
  \begin{align*}
    E(M,g) & = b_m \int_M \tensor[^g]{R}{} d\mu_g
             + \frac{m - 1}{2} c_m \int_{\partial M} \sigma_1\left( B_{\nu} \right) d\sigma_h \\
           & + c_m \int_{\partial M} \langle{\overline{Z}, u_{\ast}\nu} \rangle_{\overline{\delta}}
             \left( {\sc{h} \over 2} + \sigma_2(B_{\nu}) \right) d\sigma_h \\
           & - c_m \int_M \langle{G, \overline{B}_{\overline{Z}}} \rangle d\mu_g
             + c_m \int_{\partial M} \langle{T_1\left( B_{\nu} \right),
             \iota^{\ast}\overline{B}_{\overline{Z}}} \rangle_h d\sigma_h
  \end{align*}
  onde \( b_m = \frac{m - 2}{4} c_m \),
  \( \overline{Z} = u \in \Gamma\left( u^{\ast}T\overline{\bbR}^{d_m} \right) \).
\end{sumario}
\begin{proof}
  A identidade segue da discussão precedente.
\end{proof}

\todo[noline]{Ref.: [c3-pg66]}
\begin{remark}
  É sabido que \( \tau > \tau^{\ast} \equiv m -2 \Rightarrow E(M,g) \equiv 0 \).
  Assim, é importante verificar se a condição assintótica
  \( q \ge q_c \not\Rightarrow \tau > \tau^{\ast} \).
\end{remark}

\begin{definition}[Transformação de Newton]
  \label{def:transformaccaoNewton}
  Sejam \( (V^m, g) \) e \( (W^n, h) \) espaços vetoriais euclidianos,
  e \( B = \tensor[]{B}{_{ab}^{\alpha}} \in S^2(V) \otimes W \)
  uma forma bilinear simétrica a valores vetoriais. Para qualquer inteiro par
  \( 0 \le k \le m -1 \), a k-ésima e a (k + 1)-ésima transformação de Newton
  associada a forma bilinear \( B \), são as formas bilineares:
  \begin{itemize}
  \item \( T_k = T _{(k)ab} \in S^2(V) \)
    \begin{align*}
      T_{(k)ab} = \frac{1}{k!} g_{ac} \delta_{a_1 \ldots a_k b}^{b_1 \ldots b_k c}
      \prod_{s = 1}^{k/2} \tensor[]{B}{_{b_{2s - 1}}^{a_{2s - 1}}_{\alpha_s}}
      \tensor[]{B}{_{b_{2s}}^{a_{2s}\alpha_s}}
    \end{align*}
  \item \( T_{k - 1} = \tensor[]{T}{_{(k - 1)ab}^{\alpha}} \in S^2(V) \otimes W \),
    \begin{align*}
      \tensor[]{T}{_{(k - 1)ab}^{\alpha}} = \frac{1}{(k - 1)!} g_{ac}
      \delta_{a_1 \ldots a_{k - 1} b}^{b_1 \ldots b_{k - 1} c}
      \prod_{s = 1}^{(k - 2)/2} \tensor[]{B}{_{b_{2s - 1}}^{a_{2s - 1}}_{\alpha_s}}
      \tensor[]{B}{_{b_{2s}}^{a_{2s}\alpha_s}}
      \tensor[]{B}{_{b_{k - 1}}^{a_{k - 1} \alpha}}
    \end{align*}
  \end{itemize}
\end{definition}

\begin{definition}[Função simétrica elementar]
  \label{def:funccao_simetrica_elementar}
  Sejam \( (V^m, g) \) e \( (W^n, h) \) espaços vetoriais euclidianos,
  e \( B = \tensor[]{B}{_{ab}^{\alpha}} \in S^2(V) \otimes W \)
  uma forma bilinear a valores vetoriais. Para qualquer inteiro par
  \( 0 \le k \le m -1 \), a k-ésima e a (k + 1)-ésima
  função simétrica elementar associada a forma bilinear \( B \), são os campos:
  \begin{itemize}
  \item \( S_k \in \bbR \),
    \begin{align*}
      S_k = \frac{1}{k!} \delta_{a_1 \ldots a_k}^{b_1 \ldots b_k}
      \prod_{s = 1}^{k/2} \tensor[]{B}{_{b_{2s - 1}}^{a_{2s - 1}}_{\alpha_s}}
      \tensor[]{B}{_{b_{2s}}^{a_{2s}\alpha_s}}
    \end{align*}
  \item \( S_{k + 1} = \tensor[]{S}{_{(k + 1)}^{\alpha}} \in W \),
    \begin{align*}
      \tensor[]{S}{_{(k + 1)}^{\alpha}}
      = \frac{1}{(k + 1)!} \delta_{a_1 \ldots a_{k + 1}}^{b_1 \ldots b_{k + 1}}
      \prod_{s = 1}^{k/2} \tensor[]{B}{_{b_{2s - 1}}^{a_{2s - 1}}_{\alpha_s}}
      \tensor[]{B}{_{b_{2s}}^{a_{2s}\alpha_s}}
        \tensor[]{B}{_{b_{k + 1}}^{a_(k + 1) \alpha}}
    \end{align*}
  \end{itemize}
\end{definition}

\begin{lemma}[Identidade de Girard-Newton] \label{lema:identidade_Girard-Newton}
  Para \( 0 \le k \le m - 1 \), o traço da k-ésima transformação de Newton
  é dado pela identidade:
  \begin{align*}
    C_g T_k = (m - k) S_k.
  \end{align*}
\end{lemma}

\begin{lemma}[Expansão de Laplace] \label{lema:expansao_Laplace}
  Para qualquer inteiro par, \( 0 \le k \le m - 1 \), temos:
  \begin{align*}
    k S_k & = \tensor[]{T}{_{(k - 1)ab\alpha}} \tensor[]{B}{^{ab\alpha}} \\
    (k + 1) \tensor[]{S}{_{(k + 1)}^{\alpha}}
    & = \tensor[]{T}{_{(k)ab}} \tensor[]{B}{^{ab\alpha}}
  \end{align*}
\end{lemma}

\begin{lemma}[Equação de Gauss]
  Seja uma imersão isométrica suave
  \[
    u: (M^m, g) \hookrightarrow \left( \overline{\bbR}^{d_m}, \overline{\delta} \right).
  \]
  Então vale a equação de Gauss:
  \begin{align*}
    R_{abcd}
    = \tensor[]{\overline{B}}{_{ad}_{\alpha}} \tensor[]{\overline{B}}{_{bc}^{\alpha}}
    - \tensor[]{\overline{B}}{_{ac}_{\alpha}} \tensor[]{\overline{B}}{_{bd}^{\alpha}}
  \end{align*}
\end{lemma}

Combinando as identidades \eqref{eq:tensor_einstein}, \eqref{eq:Gauss_ambiente_chato},
com a definição de transformação de Newton \eqref{def:transformaccaoNewton}, obtemos:
\begin{align}
  \label{eq:lovelockNewtonTransformaccao}
  G_{ab} & = \frac{1}{4} g_{ac} \delta_{b j_1 j_2}^{c i_1 i_2}
    \tensor[]{R}{_{i_1 i_2}^{j_1 j_2}} \nonumber \\
         & = \frac{1}{4} g_{ac} \delta_{b j_1 j_2}^{c i_1 i_2} \left(
           \tensor[]{\overline{B}}{_{i_1}^{j_2}_{\alpha}}
           \tensor[]{\overline{B}}{_{i_2}^{j_1}^{\alpha}}
           - \tensor[]{\overline{B}}{_{i_1}^{j_1}_{\alpha}}
           \tensor[]{\overline{B}}{_{i_2}^{j_2}^{\alpha}} \right) \nonumber \\
         & = - \frac{2}{4} g_{ac} \delta_{b j_1 j_2}^{c i_1 i_2}
           \tensor[]{\overline{B}}{_{i_1}^{j_1}_{\alpha}}
           \tensor[]{\overline{B}}{_{i_2}^{j_2}^{\alpha}} \nonumber \\
         & = - T_{(2)ab}
\end{align}
Assim, combinando essa identidade, com a expansão de Laplace
\eqref{lema:expansao_Laplace}, obtemos:
\begin{align*}
  \langle{G, \overline{B}_{\overline{Z}}} \rangle_g
  = - T_{(2)ab} \tensor[]{\overline{B}}{^{ab}^{\alpha}} \overline{Z}_{\alpha}
  = - 3 \tensor[]{\overline{S}}{_{(3)}^{\alpha}} \overline{Z}_{\alpha}
  = - 3 \langle{\overline{S}_{3}, \overline{Z}}  \rangle_{\overline{\delta}}.
\end{align*}
Ainda, tomando o traço do tensor de Einstein, temos:
\[ C_g G = - \frac{m - 2}{2} \tensor[^g]{R}{}. \]
Logo, da identidade de Girard-Newton~\ref{lema:identidade_Girard-Newton} segue que,
\begin{align*}
  \tensor[^g]{R}{} = 2 \overline{S}_{2}.
\end{align*}
Dessa forma, se a hipótese assintótica sobre a estrutura riemanniana, apresentada
no sumário \ref{sumario:energia_ADM--mergulho_isometrico}, é satisfeita; se,
também se aplica a hipótese de existência de uma imersão isométrica,
com a condição assintótica correspondente; e, a hipótese adicional de bordo vazio
\( \partial M = \emptyset \) vale; então, por substituição direta desses
resultados na fórmula daquele sumário, obtemos:
\begin{align*}
  E(M,g) & = b_m \int_M \tensor[^g]{R}{} d\mu_g
           - c_m \int_M \langle{G, \overline{B}_{\overline{Z}}} \rangle_g d\mu_g \\
         & = 2 b_m \int_M \overline{S}_{2} d\mu_g
           + 3 c_m \int_M \langle{\overline{S}_{3}, \overline{Z}}
           \rangle_{\overline{\delta}} d\mu_g \\
         & = c_m \left( \frac{m - 2}{2} \int_M \overline{S}_{2} d\mu_g
           + 3 \int_M \langle{\overline{S}_{3}, \overline{Z}}
           \rangle_{\overline{\delta}} d\mu_g
            \right).
\end{align*}
Portanto, pelo teorema da massa positiva, concluímos que vale a desigualdade:
\begin{align*}
  \frac{m - 2}{2} \int_M \overline{S}_{2} d\mu_g
  \ge - 3 \int_M \langle{\overline{S}_{3}, \overline{Z}} \rangle_{\overline{\delta}}
  d\mu_g,
\end{align*}
quando \( \overline{S}_{2} \ge 0 \), com a igualdade sendo
atingida somente quando \( (M^m, g) \) é isometricamente equivalente ao espaço
euclidiano e a imersão realiza a condição integral,
\begin{align*}
  \int_M \langle{\overline{S}_{3},
  \overline{Z}} \rangle_{\overline{\delta}} d\mu_g = 0
\end{align*}

\begin{sumario}
  \label{sumario:desigualdade_curvatura_media_imersao_isometrica}
  Seja \( (M^m, g) \) uma variedade riemanniana completa,
  com bordo vazio \( \partial M = \emptyset \), assintoticamente euclidiana à taxa
  \( \protect{\tau > \tau_c = {m - 2 \over 2}} \).
  Se $M$ admite uma imersão isométrica suave
  \[
    u: (M^m, g) \hookrightarrow \left( \overline{\bbR}^d, \overline{\delta} \right)
  \]
  e uma carta nos fins, que satisfazem a condição assintótica,
  \begin{align*}
   \left( |u|_{\overline{\delta}}^2 - |x|_{\delta}^2 \right)(x) = O_1(|x|^{-q + 1}),
  \end{align*}
  com \( q \ge q_c = m -1 - (\tau_c + 2) \); e, além disso, a imersão é tal que
  \( \overline{S}_{2} \ge 0 \), então vale a desigualdade:
  \begin{align*}
    \frac{m - 2}{2} \int_M \overline{S}_{2} d\mu_g
    \ge - 3 \int_M \langle{\overline{S}_{3}, \overline{Z}}
    \rangle_{\overline{\delta}} d\mu_g,
  \end{align*}
  com a igualdade sendo atingida somente quando \( (M^m, g) \) é isometricamente
  equivalente ao espaço euclidiano e a imersão realiza a condição integral,
  \begin{align*}
    \int_M \langle{\overline{S}_{3},
    \overline{Z}} \rangle_{\overline{\delta}} d\mu_g = 0
  \end{align*}


  \begin{remark}
    Essa desigualdade é comparável com a fórmula de Hsiung-Minkowski.
  \end{remark}

  \begin{remark}
    Se pudermos provar essa desigualdade por outros métodos, esse método de prova
    estabelece mais uma prova para o teorema da massa positiva,
    válida em qualquer dimensão.
  \end{remark}

  \begin{remark}
    Essa construção sugere que, por uma analogia com a energia ADM,
    é possível construir uma família de invariantes topológico-diferenciais,
    associada as aplicações, no espaço imersões, das variedades com fins
    euclidianos, no sentido da topologia diferencial,
    num espaço riemanniano adequado.
  \end{remark}
\end{sumario}

\section{Massa GBC}

Os argumentos se aplicam as demais massas GBC.

\begin{definition}[Curvatura de Lovelock]
  \begin{align}
    \label{eq:curvaturaLovelock}
    E_k = E_{(k)ab} = \frac{1}{2^{k + 1}} g_{ac}
    \delta_{b j_1 j_2 \ldots j_{2k - 2} j_{2k}}^{c i_1 i_2 \ldots i_{2k - 1} i_{2k}}
    \prod_{s = 1}^{k} \tensor[]{R}{_{i_{2s - 1} i_{2s}}^{j_{2s - 1} j_{2s}}}
  \end{align}
\end{definition}

\begin{definition}[Massa GBC]
  \label{def:massaGBC}
  \begin{align}
    b_{n,k} m_k(M,g) := - \int_{\Sigma_{\infty}} E_k\left( X, \nu \right)
    d\sigma_g
  \end{align}
  \begin{description}
  \item[\( (M^n, g) \)] variedade riemanianna completa, assintoticamente plana
    à taxa \( \tau > \frac{m - 2k}{k + 1} \)
  \item[\( b_{n,k} \)] \( \equiv \frac{2^{k -1} (n - 1)! \omega_{n - 1}}{(n - 2k -1)!} \),
    onde \( \omega_{m-1} \equiv |\mathbf{S}^{m-1}| \)
  \item[\( X \)] \( = x^i \frac{\partial}{\partial x^i} \)
    é o campo definido nos fins, via ``pullback'' da dilatação radial
    do espaço euclidiano
  \item[\( \nu \)] campo normal unitário apontando pro infinito
  \end{description}
\end{definition}

\begin{sumario}[Passagem a um campo global]
  \label{sumario:passagemCampoGlobal}
  Considere o cenário:
  \begin{itemize}
  \item \( (M^n, g) \) completa, com \( n \ge 3 \)
  \item AE à taxa \( \tau > \tau_c, \quad \tau_c > \frac{n - 2k}{k + 1} \),
    com \( 1 \le k \le n/2 \)
  \item \( X =  x^i \partial_{x_i} \) dilatação radial numa carta admissível
  \end{itemize}
  Então, para todo campo global \( Y \in \Gamma\left( TM \right) \) que, em alguma carta admissível,
  satisfaz a condição assintótica,
  \begin{displaymath}
    \left( Y^i - X^i \right)(x) = O(|x|^{- q}), \ |x| \to \infty,
  \end{displaymath}
  com \( q \ge q_c \equiv \tau_c - 1 \), valem as identidades:
  \begin{displaymath}
    b_{n,k} \ m_k(M, g) = - \int_{\partial M_{\infty}}
    E_k\left( Y, \tensor[^g]{\nu}{} \right) d\sigma_g
    = - \frac{1}{2} \int_{M} \langle{E_k, \mathcal{L}_Y g} \rangle_g d\mu_g
  \end{displaymath}
\end{sumario}
\begin{proof}
  Argumentando como no caso da massa ADM
  (veja pg. \pageref{pg:passagemCampoGlobal}), conclui-se que para \( r >> 1 \)
  vale:
  \begin{align*}
    \left\vert \int_{S_r} E_k(Y, \nu) d\sigma_g
    - \int_{S_r} E_k(X, \nu) d\sigma_g \right\vert
    \le C(n) r^{-[ k(\tau + 2) - (n -1) + q]} (1 + r^{-\tau})^2
  \end{align*}
  Como lá, é suficiente tomarmos
  \begin{displaymath}
    q \ge q_c \equiv n - 1 - k(\tau_c + 2) = \tau_c - 1.
  \end{displaymath}
  O resto da prova segue como no sumário~\ref{sumario:energia_ADM--campo_global}.
\end{proof}

\begin{sumario}
  \label{sumario:passagemCampoGlobalReciproca}
  Reciprocamente, considere o cenário:
  \begin{itemize}
  \item \( M^n \) com fins euclidianos
  \item \( X =  x^i \partial_{x_i} \) dilatação radial num atlas dos fins
  \item \( Y \in \Gamma\left( TM \right) \) satisfazendo a condição assintótica,
    \begin{displaymath}
      \left( Y^i - X^i \right)(x) = O(|x|^{- q}), \ |x| \to \infty,
    \end{displaymath}
    num atlas dos fins, digamos \( \mcA \),
    com \( q \ge q_c = \frac{n - 1 - 3k}{k + 1}, 1 \le k \le n /2 \) inteiro.
  \end{itemize}
  Então, para toda métrica \( g \in \mcM(M) \) que,
  com respeito ao atlas \( \mcA \),
  é AE à taxa \( \tau > \tau_c = q_c + 1 \), vale a identidade:
  \begin{displaymath}
    \frac{1}{2} \int_{M} \langle{E_k, \mathcal{L}_Y g} \rangle_g d\mu_g
     = - b_{n,k} \ m_k(M, g).
  \end{displaymath}
\end{sumario}
\begin{proof}
 A prova segue os mesmos passos do sumário~\ref{sumario:passagemCampoGlobal}.
\end{proof}


\subsection{Aplicação: imersões}
\label{sec:massaGBCImersao}

\begin{definition}[Imersão assintoticamente euclidiana]
  Considere \( n \ge 3 \), \( 1 \le k < n/2 \) inteiros,
  e uma variedade \( M^n \) com fins euclidianos. Uma imersão é assintoticamente
  euclidiana à taxa \( \tau \) é uma imersão
  \( \psi: M^n \looparrowright (\bbR^d, \overline{\delta}) \) tal que, com respeito a uma carta nos fins, vale:
  \begin{itemize}
  \item \( \psi^{\ast} \overline{\delta} \) completa,
    AE à taxa \( \tau > \tau_c = \frac{n - 2k}{k + 1} \)
  \item \( \left( |\psi|_{\overline{\delta}}^2(x) - |x|^2 \right)
    = O_1(|x|^{-q}), \ |x| \to \infty, \ q \ge q_c = \tau_c - 1 \)
  \end{itemize}
\end{definition}

\begin{sumario}[Massa GBC de uma imersão]
 \label{sumario:massaGBCImersao}
 Considere o cenário:
 \begin{itemize}
 \item  \( n \ge 3 \) e \( 1 \le k \le n/2 \) inteiros
 \item  \( \psi: M^n \looparrowright (\bbR^d, \overline{\delta}) \)
   imersão AE à taxa \( \tau > \tau_c  = \frac{n - 2k}{k + 1} \)
 \item \( \overline{S}_{2k} \in L^1(M) \)
 \end{itemize}
 Então, vale a identidade integral:
 \begin{displaymath}
   \frac{2 b_{n,k}}{(2k)!} \ E_k(\psi)
   = \frac{n - 2k}{2} \int_M \overline{S}_{2k} dM
   + (2k + 1) \int_M \langle{\overline{S}_{2k + 1}, \overline{Z}}
   \rangle_{\overline{\delta}} dM
 \end{displaymath}
\end{sumario}
\begin{proof}
  A discussão que precede o sumário
  \ref{sumario:energia_ADM--formula_intrinseca--campo_gradiente},
  garante que podemos tomar o campo como na seção \ref{sec:mergulho_isometrico}.

  Argumentando como em~(\ref{eq:derivadaLie}), obtém-se:
  \begin{displaymath}
    \langle E_k, \mcL_Y \psi^{\ast} \overline{\delta} \rangle
    = tr_{\psi^{\ast} \overline{\delta}} E_k
    + 2 \langle E_k, \overline{B}_{\overline{Z}}  \rangle_{\psi^{\ast} \overline{\delta}}
  \end{displaymath}
  Por sua vez, argumentando como em~(\ref{eq:lovelockNewtonTransformaccao}),
  obtém-se:
  \begin{displaymath}
    E_{(k)ab} = - \frac{2^k}{2^{k + 1}} g_{ac}
    \delta_{b j_1 j_2 \ldots j_{2k - 2} j_{2k}}^{c i_1 i_2 \ldots i_{2k - 1} i_{2k}}
    \prod_{s = 1}^{k} \tensor[]{\overline{B}}{_{i_{2s - 1}}^{j_{2s - 1}}_{\alpha_s}}
    \tensor[]{\overline{B}}{_{i_{2s}}^{j_{2s}}^{\alpha_s}}
    = - \frac{(2k)!}{2} \ \overline{T}_{(2k)ab}
  \end{displaymath}

  Assim, aplicando a identidade de Girard-Newton
  (lema \ref{lema:identidade_Girard-Newton}) e a expansão de Laplace
  (lema \ref{lema:expansao_Laplace}), obtém-se:
  \begin{align*}
    \langle E_k, \mcL_Y \psi^{\ast} \overline{\delta} \rangle
    & = - \frac{(2k)!}{2} \left[
      tr_{\psi^{\ast} \overline{\delta}} \overline{T}_{2k}
      + 2 \langle \overline{T}_{2k},
      \overline{B}_{\overline{Z}}  \rangle_{\psi^{\ast} \overline{\delta}}
      \right] \\
    & = - \frac{(2k)!}{2} \left[ (n - 2k) \overline{S}_{2k}
      + 2(2k + 1) \langle \overline{S}_{2k + 1}, \overline{Z} \rangle
    \right]
  \end{align*}

  Substituindo essa identidade na fórmula do sumário
  \ref{sumario:passagemCampoGlobal}, o resultado segue.
\end{proof}

Nesse contexto, a conjectura da massa positiva se traduz
na seguinte conjectura na teoria das imersões.
\begin{conjectura}
  Considere uma imersão como descrita no sumário~\ref{sumario:massaGBCImersao}.
  \begin{displaymath}
    \overline{S}_{2k} \ge 0 \Longrightarrow
    \frac{n - 2k}{2} \int_M \overline{S}_{2k} dM
    \ge - (2k + 1) \int_M \langle{\overline{S}_{2k + 1}, \overline{Z}}
    \rangle_{\overline{\delta}} dM
  \end{displaymath}
  A desigualdade é saturada \( \Longleftrightarrow \psi \) é imersão isométrica
  do espaço euclidiano, tal que:
  \begin{displaymath}
    \int_M \langle \overline{S}_{2k + 1}, \overline{Z}
    \rangle_{\overline{\delta}} dM = 0.
  \end{displaymath}
\end{conjectura}

\begin{remark}
  Nesse cenário, a conjectura é válida sempre que a conjectura da massa positiva
  for válida.
\end{remark}

\end{document}